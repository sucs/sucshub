# Design Brief

This document outlines the mission statement for this project, as well as the goals for functionality and code design.

## Mission Statement

The new SUCS members hub should serve as an access point for SUCS services, it should integrate with Discord and Milliways, offer quick access to relevant news and generally help connect the society.
It should serve as a hub for useful information about the society, preferred communication streams, information on how to make use of SUCS services and encourage new members to get involved by defining clear goals.
Keep logs of SUCS history, e.g past events. 

## Desired functionality

* Game servers
* Easier hosting
* New milliways webclient
* Custom webhooks
* API for IFTTT
* Oauth provider? Via SUCS LDAP
* Access to etherpads - also with LDAP auth

# Reasons for design choices

* The service should be fast and light on resources, dotnet core is a great option for this as it is one of the fastest dynamic site hosting languages.