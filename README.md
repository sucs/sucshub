# Sucs Members Site

This website serves as a portal to SUCS members services as well as a hub to access the news, access Milliways etc.
For more detials see the [design brief spec](DESIGNBRIEF.md)

# Contributing

If you want to get stuck in have a read of [CONTRIBUTING.md](CONTRIBUTING.md), make a fork, add your changes and get yourself a merge request!

# Database

For now we're using a temporary SQLite database stored in the file `database.db`. If you want to make modifications to the database, install the application `DB Browser for SQLite` - You can find downloads and instructions [here](https://sqlitebrowser.org/dl/).