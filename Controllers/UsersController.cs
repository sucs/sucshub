using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using SucsMembers.Models;
using SucsMembers.Database;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;

namespace SucsMembers.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private SMDbContext _context;

        public UsersController(SMDbContext context)
        {
            _context = context;
        }
        [AllowAnonymous]
        public IActionResult Index() {
            if (HttpContext.User.Identity.IsAuthenticated){
              string username = HttpContext.User.Identity.Name;
              //Get the user with the same name as the cookie
              User user = _context.Users
                      .Where(u => u.Username == username)
                      .First();
              return View(user);
            }
            //Render Login.cshtml
            return View("Login");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromForm]LoginModel model)
        {
            User user = null;
            if (model.Username == null){
              //TODO replace with propper error handling
              user = _context.Users
                      .Where(u => u.Username == "whizzywig")
                      .First();
            }else{
              user = _context.Users
                      .Where(u => u.Username == model.Username)
                      .First();
            }
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Username),
                //If we want to add more claims
                //new Claim("FirstName", user.Firstname),
                //new Claim("LastName", user.Lastname),
            };

            var claimsIdentity = new ClaimsIdentity(
              claims, CookieAuthenticationDefaults.AuthenticationScheme);

              var authProperties = new AuthenticationProperties
              {
              //AllowRefresh = <bool>,
              // Refreshing the authentication session should be allowed.

              //ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
              // The time at which the authentication ticket expires. A
              // value set here overrides the ExpireTimeSpan option of
              // CookieAuthenticationOptions set with AddCookie.

              //IsPersistent = true,
              // Whether the authentication session is persisted across
              // multiple requests. When used with cookies, controls
              // whether the cookie's lifetime is absolute (matching the
              // lifetime of the authentication ticket) or session-based.

              //IssuedUtc = <DateTimeOffset>,
              // The time at which the authentication ticket was issued.

              //RedirectUri = <string>
              // The full path or absolute URI to be used as an http
              // redirect response value.
            };
            //Gives the user their cookie
            await HttpContext.SignInAsync(
              CookieAuthenticationDefaults.AuthenticationScheme,
              new ClaimsPrincipal(claimsIdentity),
              authProperties);

            //return the user back to the profile page
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Logout(){
          await HttpContext.SignOutAsync();
          return RedirectToAction("Index");
        }
    }
}
