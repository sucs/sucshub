This document outlines the best way to contribute to this project.

**NOTE:** As this project is hosted on SUCS GitLab, you'll need to [become a member of the society](https://join.sucs.org) and get an account set up!

1. [Fork the project](https://projects.sucs.org/sucs/sucshub/-/forks/new)
2. Create a new branch on your fork
3. Create a merge request (a handy dandy button will appear at the top of your fork page after your've created a new fork).

**Important:** If you aren't super confident with git, tick the box at the bottom of the merge request that says "Allow commits from members who can merge to the target branch". So if you run into trouble with your git history someone can help you out.

# Formatting Commit messgaes

Commit messages should start with the name of the *thing* that is being modified, e.g:
```
"[Home Page] Fix the logo being rotated by 90 degrees"
```

You should also include lots of details in the commit, use `git commit` and your text editor for this to add extra details if needed.

Finally, with few exceptions, commits should represent one change or a group of small changes, if you have made lots of changes without comitting then take a look at using [git interactive mode to commit part of a file](https://filip-prochazka.com/blog/git-commit-only-parts-of-a-file).

You can also stage individual files for commit with

```
git add ./path/to/file
```

Use common sense to decide whether to split commits - but it's generally better to split a commit if you're unsure.