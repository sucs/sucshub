namespace SucsMembers.Models
{
    public class User {
        public int UserId {get;set;}
        public string GUID {get;set;}

        public string Username {get;set;}
        public string Firstname {get;set;}
        public string Lastname {get;set;}
        public ulong DiscordId {get;set;}
    }
    public class LoginModel {
        public string Username {get;set;}
        public string Password {get;set;}
    }
}
